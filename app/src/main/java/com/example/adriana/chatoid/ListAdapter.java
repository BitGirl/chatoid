package com.example.adriana.chatoid;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by adriana on 4/2/2016.
 */
public class ListAdapter extends ArrayAdapter<String> {

    HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

    public ListAdapter(Context context, int textViewResourceId,
                              List<String> objects) {
        super(context, textViewResourceId, objects);
        for (int i = 0; i < objects.size(); ++i) {
            mIdMap.put(objects.get(i), i);
        }
    }

    @Override
    public View getView(int position, View convertView,
                        ViewGroup parent) {
        View view =super.getView(position, convertView, parent);

        TextView textView=(TextView) view.findViewById(android.R.id.text1);

            /*YOUR CHOICE OF COLOR*/
        textView.setTextColor(Color.parseColor("#ffffff"));
        textView.setTextSize(20.0f);

        return view;
    }

    @Override
    public long getItemId(int position) {
        String item = getItem(position);
        return mIdMap.get(item);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    public void addList(List<String> messages) {
        mIdMap.clear();
        for (int i = 0; i < messages.size(); ++i) {
            mIdMap.put(messages.get(i), i);
        }
    }

}