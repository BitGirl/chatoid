package com.example.adriana.chatoid;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;

import com.facebook.Profile;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ConversationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int FILE_SELECT_CODE = 0;
    private String destinatarID;
    private String destinatarNAME;
    private String currentUserID;
    private String currentUserNAME;
    private ListView listview;
    private ArrayList<String> messageList = new ArrayList<String>();
    private ListAdapter adapter;
    private Runnable runClass;
    private Handler handler;
    private boolean running = true;
    private String path = "";
    private Integer inc = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_conversation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        /**
         * when conversation activity is created
         * some information are past through intent
         */
        Intent intent = getIntent();
        destinatarID = intent.getStringExtra("friendID");
        destinatarNAME = intent.getStringExtra("friendNAME");
        /**
         * get current user information
         */
        Profile profile = Profile.getCurrentProfile();
        currentUserID = profile.getId();
        currentUserNAME = profile.getName();

        listview = (ListView) findViewById(R.id.listView2);
        adapter = new ListAdapter(this, android.R.layout.simple_list_item_1, messageList);
        /**
         * get all conversations between current user and receiver
         */
        new HttpRequestClass().execute("getAllConversations", currentUserID, destinatarID);

        final EditText messageEditText = (EditText)findViewById(R.id.editText);
        assert messageEditText != null;
        messageEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                /**
                 * when enter key is pressed the message is inserted
                 * in the list view and send to the server for database insertion
                 */
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() != KeyEvent.ACTION_DOWN) {
                    messageList.add(currentUserNAME + ": " + messageEditText.getText().toString());
                    adapter.addList(messageList);
                    adapter.notifyDataSetChanged();
                    listview.setAdapter(adapter);
                    messageEditText.getText().clear();
                    Log.d("Enter", messageList.toString());
                    new HttpRequestClass().execute("setConversation",  currentUserID, destinatarID,
                            messageList.get(messageList.size() - 1).substring(messageList.get(messageList.size() - 1).indexOf(": ") + 2));
                    return true;
                }
                return false;
            }
        });

        Button uploadButton = (Button)findViewById(R.id.uploadButton);
        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Insert some message if there is no File Manager on the device
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    Log.d("URI", "File Uri: " + uri.toString());
                    path = uri.getPath();
                    final EditText messageEditText = (EditText)findViewById(R.id.editText);
                    messageEditText.setText(uri.toString());
                    // Get the path
                    String path = null;
                    try {
                        path = FileUtils.getPath(this, uri);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    Log.d("PATH", "File Path: " + path);
                    // Get the file instance
                    // File file = new File(path);
                    // Initiate the upload
                    new UploadFileClass().execute("");
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();
        running = true;
        // verify if there are some messages for the current user
        handler = new Handler();
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                runClass = new Runnable() {
                    public void run() {
                       new HttpRequestClass().execute("getAllConversations", currentUserID, destinatarID);
                    }
                };
                handler.post(runClass);
            }
        };
        timer.schedule(task, 0, 2000);
    }

    @Override
    protected void onStop() {
        super.onStop();
        running = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        running = false;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();
        // display Friend Activity and take the list of friends using Facebook API
        if (id == R.id.nav_friends) {
            GraphRequestAsyncTask graphRequestAsyncTask = new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/me/friends",
                    null,
                    com.facebook.HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            Intent intent = new Intent(getApplicationContext(), FriendActivity.class);
                            try {
                                JSONArray rawName = response.getJSONObject().getJSONArray("data");
                                intent.putExtra("jsondata", rawName.toString());
                                Log.d("friends:", rawName.toString());
                                startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).executeAsync();
        } else if (id == R.id.nav_groups) {
            Intent i = new Intent(getApplicationContext(), GroupActivity.class);
            startActivity(i);
        }else if (id == R.id.nav_notifications) {
            Intent i = new Intent(getApplicationContext(), NotificationActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * These class is used to execute REST service asynchronously
     * It can take string arguments and it can return an object of type Conversation
     */
    public class HttpRequestClass extends AsyncTask<String, Void, Conversation> {

        @Override
        protected Conversation doInBackground(String... params) {
            while(running) {
                // execute POST to send conversation to server
                if (params[0].equals("setConversation")) {
                    try {
                        final String url = Constants.ServerIP + "conversation";
                        RestTemplate restTemplate = new RestTemplate();
                        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                        Conversation conversation = restTemplate.postForObject(url, new Conversation(params[1], params[2], params[3]), Conversation.class);
                        return conversation;
                    } catch (Exception e) {
                        Log.e("ConversationActivity", e.getMessage(), e);
                    }
                }
                // execute POST to take all the messages for the current conversation from server
                else if (params[0].equals("getAllConversations")) {
                    try {
                        final String url = Constants.ServerIP + "getAllConversations";
                        RestTemplate restTemplate = new RestTemplate();
                        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                        Conversation conversation = restTemplate.postForObject(url, new Conversation(params[1], params[2], ""), Conversation.class);
                        return conversation;
                    } catch (Exception e) {
                        Log.e("ConversationActivity", e.getMessage(), e);
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Conversation conversation) {
            if(conversation != null && conversation.getContent() != "") {
                // take all messages for the current conversation
                // and insert them to the list view
                if(currentUserID.equals(conversation.getExpeditor()) &&
                        destinatarID.equals(conversation.getDestinatar())){
                    messageList.clear();
                    for (int i = 0; i < conversation.getContent().split(";").length; i +=2 ) {
                        String text = "";
                        if(conversation.getContent().split(";")[i].equals(currentUserID)){
                            text += currentUserNAME + ": " + conversation.getContent().split(";")[i + 1];
                        }
                        else if(conversation.getContent().split(";")[i].equals(destinatarID)){
                            text += destinatarNAME + ": " + conversation.getContent().split(";")[i + 1];
                        }
                        messageList.add(text);
                    }
                    adapter.addList(messageList);
                    adapter.notifyDataSetChanged();
                    listview.setAdapter(adapter);
                }
            }
        }
    }

    public class UploadFileClass extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {

            FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
            formHttpMessageConverter.setCharset(Charset.forName("UTF8"));
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(formHttpMessageConverter);
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());

            String uri = Constants.ServerIP + "uploadPhoto/" + inc;
            inc++;

            MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
            map.add("expenseId", inc.toString());
            map.add("file", new FileSystemResource("/storage/emulated/0/Download/AVON (2).pdf"));

            HttpHeaders imageHeaders = new HttpHeaders();
            imageHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

            HttpEntity<MultiValueMap<String, Object>> imageEntity = new HttpEntity<MultiValueMap<String, Object>>(map, imageHeaders);

            restTemplate.exchange(uri, HttpMethod.POST, imageEntity, Boolean.class);


            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(ConversationActivity.this, "File Uploaded!",
                    Toast.LENGTH_LONG).show();
        }
    }
}
