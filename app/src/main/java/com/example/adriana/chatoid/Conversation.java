package com.example.adriana.chatoid;

/**
 * Created by adriana on 3/13/2016.
 */
public class Conversation {
    private String expeditor;
    private String destinatar;
    private String content;

    public Conversation() {
    }

    public Conversation(String expeditor, String destinatar, String content) {
        this.expeditor = expeditor;
        this.destinatar = destinatar;
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getExpeditor() {
        return expeditor;
    }

    public void setExpeditor(String expeditor) {
        this.expeditor = expeditor;
    }

    public String getDestinatar() {
        return destinatar;
    }

    public void setDestinatar(String destinatar) {
        this.destinatar = destinatar;
    }
}
