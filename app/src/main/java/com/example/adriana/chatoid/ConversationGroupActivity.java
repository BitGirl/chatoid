package com.example.adriana.chatoid;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ConversationGroupActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int FILE_SELECT_CODE = 0;
    private ListView listview;
    private ArrayList<String> list = new ArrayList<String>();
    private ListAdapter adapter;
    private String groupID;
    private String currentUserID;
    private String currentUserNAME;
    private String membersID;
    private JSONArray friendslist;
    private String jsondata;
    private boolean running;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_conversation_group);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Intent intent = getIntent();
        groupID = intent.getStringExtra("groupID");

        Profile profile = Profile.getCurrentProfile();
        currentUserID = profile.getId();
        membersID += currentUserID + ";";

        GraphRequestAsyncTask graphRequestAsyncTask = new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/friends",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            JSONArray rawName = response.getJSONObject().getJSONArray("data");
                            jsondata = rawName.toString();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).executeAsync();
        try {
            if(jsondata != null)
                friendslist = new JSONArray(jsondata);
            if(friendslist != null)
                for (int i=0; i < friendslist.length(); i++) {
                    //list.add(friendslist.getJSONObject(i).getString("name"));
                }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        currentUserNAME = profile.getName();

        listview = (ListView) findViewById(R.id.listView4);
        adapter = new ListAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listview.setAdapter(adapter);

        Button addFriendButton = (Button)findViewById(R.id.addFriendToGroup);
        addFriendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), AddFriendActivity.class);
                i.putExtra("groupID", groupID);
                i.putExtra("jsondata", jsondata);
                startActivity(i);
            }
        });

        Button leaveGroupButton = (Button)findViewById(R.id.leaveGroup);
        assert leaveGroupButton != null;
        leaveGroupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), LeaveGroupActivity.class);
                i.putExtra("leaveUserID", currentUserID);
                i.putExtra("groupID", groupID);
                startActivity(i);
            }
        });

        Button uploadButton = (Button)findViewById(R.id.uploadgFile);
        assert uploadButton != null;
        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        final EditText messageEditText = (EditText)findViewById(R.id.editText2);
        assert messageEditText != null;

        messageEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() != KeyEvent.ACTION_DOWN) {
                    list.add(currentUserNAME + ": " + messageEditText.getText().toString());
                    new HttpRequestClass().execute("addGroupConversation", groupID, currentUserNAME + ": " +
                            messageEditText.getText().toString());
                    adapter.addList(list);
                    adapter.notifyDataSetChanged();
                    listview.setAdapter(adapter);
                    messageEditText.getText().clear();
                    Log.d("Enter", list.toString());
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    Log.d("URI", "File Uri: " + uri.toString());
                    final EditText messageEditText = (EditText)findViewById(R.id.editText2);
                    messageEditText.setText(uri.toString());
                    // Get the path
                    String path = null;
                    try {
                        path = FileUtils.getPath(this, uri);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    Log.d("PATH", "File Path: " + path);
                    // Get the file instance
                    // File file = new File(path);
                    // Initiate the upload
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_friends) {
            GraphRequestAsyncTask graphRequestAsyncTask = new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/me/friends",
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            Intent intent = new Intent(getApplicationContext(), FriendActivity.class);
                            try {
                                JSONArray rawName = response.getJSONObject().getJSONArray("data");
                                intent.putExtra("jsondata", rawName.toString());
                                Log.d("friends:", rawName.toString());
                                startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).executeAsync();
        } else if (id == R.id.nav_groups) {
            Intent i = new Intent(getApplicationContext(), GroupActivity.class);
            startActivity(i);
        } else if(id == R.id.nav_members) {
            Intent i = new Intent(getApplicationContext(), MembersActivity.class);
            i.putExtra("groupID", groupID);
            i.putExtra("jsondata", jsondata);
            startActivity(i);
        }else if (id == R.id.nav_notifications) {
            Intent i = new Intent(getApplicationContext(), NotificationActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        running = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        running = true;
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        new HttpRequestClass().execute("getGroupConversation", groupID, "");

                    }
                });
            }
        };
        timer.schedule(task, 0, 2000);
    }

    public class HttpRequestClass extends AsyncTask<String, Void, GroupConversation> {

        @Override
        protected GroupConversation doInBackground(String... params) {

            // execute POST to send conversation to server
            while(running) {
                if (params[0].equals("addGroupConversation")) {
                    try {
                        final String url = Constants.ServerIP + "addGroupConversation";
                        RestTemplate restTemplate = new RestTemplate();
                        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                        GroupConversation conversation = restTemplate.postForObject(url,
                                new GroupConversation(Integer.parseInt(params[1]), params[2]), GroupConversation.class);
                        return conversation;
                    } catch (Exception e) {
                        Log.e("GroupConversation", e.getMessage(), e);
                    }
                } else if (params[0].equals("getGroupConversation")) {
                    try {
                        final String url = Constants.ServerIP + "getGroupConversation";
                        RestTemplate restTemplate = new RestTemplate();
                        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                        GroupConversation conversation = restTemplate.postForObject(url,
                                new GroupConversation(Integer.parseInt(params[1]), params[2]), GroupConversation.class);
                        return conversation;
                    } catch (Exception e) {
                        Log.e("GroupConversation", e.getMessage(), e);
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(GroupConversation conversation) {
            if(conversation != null) {
                String[] conversations = conversation.getContent().split(";");
                list.clear();
                for(int i = 0; i < conversations.length; i++){
                    list.add(conversations[i]);
                    adapter.addList(list);
                    adapter.notifyDataSetChanged();
                    listview.setAdapter(adapter);
                }

            }

        }
    }
}
