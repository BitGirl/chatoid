package com.example.adriana.chatoid;

public class GroupConversation {

    private int id;
    private int grupID;
    private String content;

    public GroupConversation() {
        super();
    }

    public GroupConversation(int grupID, String content) {
        super();
        this.grupID = grupID;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGrupID() {
        return grupID;
    }

    public void setGrupID(int grupID) {
        this.grupID = grupID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "GroupConversation [grupID=" + grupID + ", content=" + content + "]";
    }
}
