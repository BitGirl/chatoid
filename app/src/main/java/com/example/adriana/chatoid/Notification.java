package com.example.adriana.chatoid;

public class Notification {

	private int id;
	private String user;
	private String content;
	
	public Notification() {
		super();

	}
	
	public Notification(String user, String content) {
		super();
		this.user = user;
		this.content = content;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getUser() {
		return user;
	}
	
	public void setUser(String user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Notification [user=" + user + ", content=" + content + "]";
	}
	
	
}
