package com.example.adriana.chatoid;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.facebook.Profile;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class LeaveGroupActivity extends AppCompatActivity {

    public String leaveUserID;
    public String groupID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_group);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent leaveIntent = getIntent();
        leaveUserID = leaveIntent.getStringExtra("leaveUserID");
        groupID = leaveIntent.getStringExtra("groupID");

        Button noButton = (Button)findViewById(R.id.buttonNo);
        assert noButton != null;
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ConversationGroupActivity.class);
                startActivity(i);
            }
        });

        Button yesButton = (Button)findViewById(R.id.buttonYes);
        assert yesButton != null;
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new HttpRequestClass().execute("leaveGroup", groupID, leaveUserID);
                Intent i = new Intent(getApplicationContext(), GroupActivity.class);
                startActivity(i);
            }
        });
    }

    public class HttpRequestClass extends AsyncTask<String, Void, GroupMembers> {

        @Override
        protected GroupMembers doInBackground(String... params) {

            // execute POST to send conversation to server
            if(params[0].equals("leaveGroup") ) {
                try {
                    final String url = Constants.ServerIP + "leaveGroup";
                    RestTemplate restTemplate = new RestTemplate();
                    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                    GroupMembers member = restTemplate.postForObject(url, new GroupMembers(params[1], params[2]), GroupMembers.class);
                    return member;
                } catch (Exception e) {
                    Log.e("LeaveGActivity", e.getMessage(), e);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(GroupMembers result) {
            if(result != null) {
                Log.d("Leave Group: ", result.toString());
            }
        }
    }

}
