package com.example.adriana.chatoid;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ListView;

import com.facebook.Profile;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

public class MembersActivity extends AppCompatActivity {

    public ArrayList<String> list = new ArrayList<String>();
    public ListAdapter adapter = null;
    public String groupID;
    public ListView listview;
    public String jsondata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_members);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listview = (ListView) findViewById(R.id.members);

        Bundle extras = getIntent().getExtras();

        jsondata = extras.getString("jsondata");

        groupID = extras.getString("groupID");
        new HttpRequestClass().execute("getMembers", groupID);

        adapter = new ListAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listview.setAdapter(adapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public class HttpRequestClass extends AsyncTask<String, Void, Result> {

        @Override
        protected Result doInBackground(String... params) {

            // execute POST to send conversation to server
            if(params[0].equals("getMembers") ) {
                try {
                    final String url = Constants.ServerIP + "getMembers";
                    RestTemplate restTemplate = new RestTemplate();
                    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                    Result groups = restTemplate.postForObject(url, new Result(params[1]), Result.class);
                    return groups;
                } catch (Exception e) {
                    Log.e("MembersActivity", e.getMessage(), e);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Result result) {
            if(result != null) {
                Log.d("Members: ", result.getResult());
                for(int i = 0; i < result.getResult().split(" ").length; i++) {
                    JSONArray friendslist = null;

                    if(jsondata != null)
                        try {
                            friendslist = new JSONArray(jsondata);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    for (int j=0; j < friendslist.length(); j++) {
                        try {
                            if(friendslist.getJSONObject(j).getString("id").equals(result.getResult().split(" ")[i]))
                                list.add(friendslist.getJSONObject(j).getString("name"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
                Profile profile = Profile.getCurrentProfile();
                list.add(profile.getName());
                adapter.addList(list);
                adapter.notifyDataSetChanged();
                listview.setAdapter(adapter);
            }
        }
    }
}
