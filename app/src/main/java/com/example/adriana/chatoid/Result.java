package com.example.adriana.chatoid;

/**
 * Created by adriana on 4/24/2016.
 */
public class Result {

    private String result;

    public Result() {
    }

    public Result(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }



}
