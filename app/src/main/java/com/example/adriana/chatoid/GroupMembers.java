package com.example.adriana.chatoid;

/**
 * Created by adriana on 4/24/2016.
 */
public class GroupMembers {

    private String groupID;
    private String memberID;

    public GroupMembers() {
    }

    public GroupMembers(String groupID, String memberID) {
        this.groupID = groupID;
        this.memberID = memberID;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    @Override
    public String toString() {
        return "GroupMembers{" +
                "groupID=" + groupID +
                ", memberID='" + memberID + '\'' +
                '}';
    }
}
