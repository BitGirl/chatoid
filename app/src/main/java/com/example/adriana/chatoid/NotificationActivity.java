package com.example.adriana.chatoid;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ListView listview;
    private ListAdapter adapter;
    private ArrayList<String> list;
    private String currentUserID;
    private String currentUserNAME;
    private String[] notifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_notification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Profile profile = Profile.getCurrentProfile();
        currentUserID = profile.getId();
        currentUserNAME = profile.getName();

        listview = (ListView) findViewById(R.id.listNotification);

        list = new ArrayList<String>();
        adapter = new ListAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), ContentNotificationActivity.class);
                i.putExtra("notification",  "You have been added to group " +
                        Constants.groups[Integer.parseInt(notifications[position]) - 1]);
                startActivity(i);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        new HttpRequestClass().execute("getNotifications", currentUserID);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_friends) {
            GraphRequestAsyncTask graphRequestAsyncTask = new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/me/friends",
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            Intent intent = new Intent(getApplicationContext(), FriendActivity.class);
                            try {
                                JSONArray rawName = response.getJSONObject().getJSONArray("data");
                                intent.putExtra("jsondata", rawName.toString());
                                Log.d("friends:", rawName.toString());
                                startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).executeAsync();
        } else if (id == R.id.nav_groups) {
            Intent i = new Intent(getApplicationContext(), GroupActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_notifications) {
            Intent i = new Intent(getApplicationContext(), NotificationActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public class HttpRequestClass extends AsyncTask<String, Void, Notification> {

        @Override
        protected Notification doInBackground(String... params) {

            // execute POST to send conversation to server
            if(params[0].equals("getNotifications") ) {
                try {
                    final String url = Constants.ServerIP + "getNotifications";
                    RestTemplate restTemplate = new RestTemplate();
                    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                    Notification notifications = restTemplate.postForObject(url, new Notification(params[1], ""), Notification.class);
                    return notifications;
                } catch (Exception e) {
                    Log.e("NotificationActivity", e.getMessage(), e);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Notification result) {
            if(result != null) {
                Log.d("Members: ", result.toString());
                notifications = result.getContent().split(";");
                list.clear();
                for(int i = 1; i <= notifications.length; i++){
                    list.add("Notification " + i);
                    adapter.addList(list);
                    adapter.notifyDataSetChanged();
                    listview.setAdapter(adapter);
                }
            }
        }
    }
}
