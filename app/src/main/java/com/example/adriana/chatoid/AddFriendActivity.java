package com.example.adriana.chatoid;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

public class AddFriendActivity extends AppCompatActivity {

    String groupID;
    JSONArray friendslist;
    String member;
    String jsondata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final AutoCompleteTextView nameTextView = (AutoCompleteTextView) findViewById(R.id.friendName);
        Button addFriendButton = (Button) findViewById(R.id.addFriend);

        Intent intent = getIntent();
        groupID = intent.getStringExtra("groupID");
        jsondata = intent.getStringExtra("jsondata");
        assert addFriendButton != null;
        addFriendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    friendslist = new JSONArray(jsondata);
                    for (int i=0; i < friendslist.length(); i++) {
                        if(friendslist.getJSONObject(i).getString("name").equals(nameTextView.getText().toString()))
                            member = friendslist.getJSONObject(i).getString("id");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                new HttpRequestClass().execute("addMember", groupID, member);
                Intent i = new Intent(getApplicationContext(), ConversationGroupActivity.class);
                startActivity(i);
            }
        });

        Button cancelButton = (Button) findViewById(R.id.cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ConversationGroupActivity.class);
                startActivity(i);
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    public class HttpRequestClass extends AsyncTask<String, Void, GroupMembers> {

        @Override
        protected GroupMembers doInBackground(String... params) {

            // execute POST to send conversation to server
            if(params[0].equals("addMember") ) {
                try {
                    final String url = Constants.ServerIP + "addMember";
                    RestTemplate restTemplate = new RestTemplate();
                    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                    GroupMembers member = restTemplate.postForObject(url, new GroupMembers(params[1], params[2]), GroupMembers.class);
                    return member;
                } catch (Exception e) {
                    Log.e("AddFriendActivity", e.getMessage(), e);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(GroupMembers result) {
            if(result != null) {
                Log.d("Add Member: ", result.toString());
            }
        }
    }

}
