package com.example.adriana.chatoid;

import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by adriana on 5/15/2016.
 */
public class ContentNotificationActivityTest extends
        ActivityInstrumentationTestCase2<ContentNotificationActivity> {

    private static final String notification = "Notification1";

    public ContentNotificationActivityTest() {
        super(ContentNotificationActivity.class);
    }


    @UiThreadTest
    public void testSetText() throws Exception {
        ContentNotificationActivity activity = getActivity();

        // search for the textView
        TextView notificationTextView = (TextView) activity.findViewById(R.id.notificationText);

        notificationTextView .setText(notification);
        assertEquals("Text incorrect", notification, notificationTextView.getText().toString());
    }


}
