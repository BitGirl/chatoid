package com.example.adriana.chatoid;

import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by adriana on 5/15/2016.
 */
public class ConversationActivityTest extends
        ActivityInstrumentationTestCase2<ConversationActivity> {

    public ConversationActivityTest() {
        super(ConversationActivity.class);
    }

    @UiThreadTest
    public void testButtonClick() throws Exception {
        ConversationActivity activity = getActivity();
        final EditText textInsert = (EditText) activity.findViewById(R.id.editText);

        activity.runOnUiThread(new Runnable() {
            public void run() {
                textInsert.setText("hello");
            }
        });

        // Check if the EditText is properly set:
        assertEquals("Text not set", "hello", textInsert.getText().toString());
    }
}
