package com.example.adriana.chatoid;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.test.UiThreadTest;
import android.test.ViewAsserts;
import android.widget.AutoCompleteTextView;

/**
 *
 */
public class AddFriendActivityTest extends
        ActivityInstrumentationTestCase2<AddFriendActivity> {

    private static final String friend = "Friend Name";

    public AddFriendActivityTest() {
        super(AddFriendActivity.class);
    }

    @UiThreadTest
    public void testSetText() throws Exception {

        AddFriendActivity activity = getActivity();

        // search for the textView
        final AutoCompleteTextView nameTextView = (AutoCompleteTextView)activity.findViewById(R.id.friendName);

        nameTextView.setText(friend);
        assertEquals("Text incorrect", friend, nameTextView.getText().toString());
    }

}