package com.example.adriana.chatoid;

import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by adriana on 5/15/2016.
 */
public class NotificationActivityTest extends
        ActivityInstrumentationTestCase2<ContentNotificationActivity> {

     public NotificationActivityTest() {
        super(ContentNotificationActivity.class);
    }

    @UiThreadTest
    public void testButtonClick() throws Exception {
        ContentNotificationActivity activity = getActivity();
        Button button = (Button) activity.findViewById(R.id.acceptButton);
        assertEquals("Button not clicked", true, button.performClick());
    }

}
