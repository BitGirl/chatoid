package com.example.adriana.chatoid;

import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.widget.EditText;

/**
 * Created by adriana on 5/15/2016.
 */
public class ConversationGroupActivityTest extends
        ActivityInstrumentationTestCase2<ConversationGroupActivity> {

    public ConversationGroupActivityTest() {
        super(ConversationGroupActivity.class);
    }

    @UiThreadTest
    public void testButtonClick() throws Exception {
        ConversationGroupActivity activity = getActivity();
        final EditText textInsert = (EditText) activity.findViewById(R.id.editText2);

        activity.runOnUiThread(new Runnable() {
            public void run() {
                textInsert.setText("hello");
            }
        });

        // Check if the EditText is properly set:
        assertEquals("Text not set", "hello", textInsert.getText().toString());
    }
}
