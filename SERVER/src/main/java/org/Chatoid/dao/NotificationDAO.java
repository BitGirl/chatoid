package org.Chatoid.dao;

import java.util.List;

import org.Chatoid.model.GroupMembers;
import org.Chatoid.model.Notification;
import org.Chatoid.utils.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

public class NotificationDAO {
	public void addNotification(Notification notification){
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    session.beginTransaction();
	    session.save(notification);
	    session.getTransaction().commit();
	    session.close();	
	}
	
	public List<Notification> getNotifications(String user){
		System.out.println("Get members: " + user);
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        String sql = " FROM Notification where user = :user ";
        Query query = session.createQuery(sql);
        query.setParameter("user", user);
                
        @SuppressWarnings("unchecked")
		List<Notification> list = query.list();   
        System.out.println("Notifications: " + list.toString());        
        session.close();    
        return list;
	}
}
