package org.Chatoid.dao;

import java.util.List;
import org.Chatoid.utils.HibernateUtil;
import org.Chatoid.model.*;
import org.hibernate.Query;
import org.hibernate.Session;

public class ConversationDAO {

	public List<Conversation> getConversations(String expeditor, String destinatar){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        String sql = "FROM Conversation c where (c.expeditor = :expeditor "
        		+ "and c.destinatar = :destinatar) or (c.destinatar = :expeditor "
        		+ "and c.expeditor = :destinatar)";
        Query query = session.createQuery(sql);
        query.setParameter("expeditor", expeditor);
        query.setParameter("destinatar", destinatar);
        
        @SuppressWarnings("unchecked")
		List<Conversation> list = query.list();
        System.out.println(expeditor + " " + destinatar);
        System.out.println("Conversations: " + list.toString());        
        session.close();    
        return list;     
	}
	
	public void addConversation(Conversation conversation){
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    session.beginTransaction();
	    session.save(conversation);
	    session.getTransaction().commit();
	    session.close();	
	}
}
