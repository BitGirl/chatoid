package org.Chatoid.dao;

import java.util.List;

import org.Chatoid.model.GroupMembers;
import org.Chatoid.utils.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

public class GroupMembersDAO {
	public List<GroupMembers> getGroups(String memberID){
		System.out.println("memberID: " + memberID);
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        String sql = " FROM GroupMembers where memberID = :memberID ";
        Query query = session.createQuery(sql);
        query.setParameter("memberID", memberID);
                
        @SuppressWarnings("unchecked")
		List<GroupMembers> list = query.list();   
        System.out.println("Groups: " + list.toString());        
        session.close();    
        return list;
	}
	
	public void addMember(GroupMembers member){
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    session.beginTransaction();	    
	    session.save(member);
	    session.getTransaction().commit();
	    session.close();	
	}
	
	public void removeMember(GroupMembers member){
		System.out.println("Member to remove: " + member.toString());
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    session.beginTransaction();	 
	    
	    String sql = "delete GroupMembers where groupID = :groupID and memberID = :memberID";
	    Query query = session.createQuery(sql);
	    query.setParameter("groupID", member.getGroupID());
	    query.setParameter("memberID", member.getMemberID());
	    query.executeUpdate();
	    
	    session.getTransaction().commit();
	    session.close();	
	}
	
	public List<GroupMembers> getMembers(String groupID){
		System.out.println("Get members: " + groupID);
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        String sql = " FROM GroupMembers where groupID = :groupID ";
        Query query = session.createQuery(sql);
        query.setParameter("groupID", groupID);
                
        @SuppressWarnings("unchecked")
		List<GroupMembers> list = query.list();   
        System.out.println("Groups: " + list.toString());        
        session.close();    
        return list;
	}
}
