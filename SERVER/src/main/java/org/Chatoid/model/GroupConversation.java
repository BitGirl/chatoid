package org.Chatoid.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GroupConversation")
public class GroupConversation {
	@Id
	@Column(name="id")
	private int id; 
	@Column(name="grup_id")
	private int grupID;
	@Column(name="content")
    private String content;	

	public GroupConversation() {
		super();
	}
	
	public GroupConversation(int grupID, String content) {
		super();
		this.grupID= grupID;
		this.content = content;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getGrupID() {
		return grupID;
	}
	
	public void setGrupID(int grupID) {
		this.grupID = grupID;
	}	

	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	@Override
	public String toString() {
		return "GroupConversation [grupID=" + grupID + ", content=" + content + "]";
	}	
}
