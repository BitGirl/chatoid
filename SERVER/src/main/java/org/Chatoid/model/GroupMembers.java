package org.Chatoid.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="groupMembers")
public class GroupMembers {

	@Id
	@Column(name="id")
	private int id;
	@Column(name="groupID")
    private String groupID;
	@Column(name="memberID")
    private String memberID;

    public GroupMembers() {
    }

    public GroupMembers(String groupID, String memberID) {
        this.groupID = groupID;
        this.memberID = memberID;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    @Override
    public String toString() {
        return "GroupMembers{" +
                "groupID=" + groupID +
                ", memberID='" + memberID + '\'' +
                '}';
    }
}

